
## Name
Website Project for UMKC COMP_SCI-490WD.

## Description
Basic personal website for a college course using a HTML/CSS template.

## Installation
Download all then run index.html in your preferred browser.


## Authors and acknowledgment
Website template can be found below in License section.

Thank you to Gabriel Morais (https://gitlab.com/comp_sci-490wd/personal-website) for uploading the entire template and all of its subsequent folders.  This saved me from having to upload multiple folders without messing up the indexing as I don't believe Git will let you upload entire folders at a time - if this can in fact be done please let me know!  

## License
 => FREE HTML TEMPLATE LICENSE BY HTML Codex

 All free HTML templates by HTML Codex are licensed under a Creative Commons Attribution 4.0 International License which means you are not allowed to remove the author’s credit link/attribution link/backlink.

 When you download or use our free HTML templates, it will attribute the following conditions.


 => YOU ARE ALLOWED

      1. You are allowed to use for your personal and commercial purposes.

      2. You are allowed to modify/customize however you like.

      3. You are allowed to convert/port for use for any CMS.

      4. You are allowed to share/distribute under the HTML Codex brand name.

      5. You are allowed to put a screenshot or a link on your blog posts or any other websites.


 => YOU ARE NOT ALLOWED

      1. You are not allowed to remove the author’s credit link/attribution link/backlink without purchasing Credit Removal License ( https://htmlcodex.com/credit-removal ).

      2. You are not allowed to sell, resale, rent, lease, license, or sub-license.

      3. You are not allowed to upload on your template websites or template collection websites or any other third party websites without our permission.

 This license can be terminated if you breach any of these conditions.

 Please contact us (https://htmlcodex.com/contact) if you have any query.

 => PURCHASE CREDIT REMOVAL LICENSE ( https://htmlcodex.com/credit-removal )

## Project status
In progress.
